var express = require('express');
var app = express();

var server = app.listen(3000, listen);

function listen() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Listening at http://' + host + ':' + port);
}

app.use(express.static('public'));

var fs = require('fs');

var data = {
  alphabet: 'abcdefghijklmnopqrstuvwxyz',
  frequencies: {
    a: 8.167,
    b: 1.492,
    c: 2.782,
    d: 4.253,
    e: 12.702,
    f: 2.228,
    g: 2.015,
    h: 6.094,
    i: 6.966,
    j: 0.153,
    k: 0.772,
    l: 4.025,
    m: 2.406,
    n: 6.749,
    o: 7.507,
    p: 1.929,
    q: 0.095,
    r: 5.987,
    s: 6.327,
    t: 9.056,
    u: 2.758,
    v: 0.978,
    w: 2.360,
    x: 0.150,
    y: 1.974,
    z: 0.074
  }
};

app.get('/all', showAll);

function showAll(req, res) {
  res.send(data);
}

app.get('/search/:letter', showOne);


app.post('/login', (req, res) => {
  var userData = req.body();
  if (userData['username'] == 'mr.bear' && userData['password'] == 'password') {
    res.status(200).end(JSON.stringify({
      status: 200,
      result: true,
      payload: {},
      message: "Login is completed"
    }));
  }
});

app.post('/register', (req, res) => {
  var userData = req.body();
  if (userData['username'] == 'mr.bear' && userData['password'] == 'password') {
    res.status(200).end(JSON.stringify({
      status: 200,
      result: true,
      payload: {
        'username' : userData['username'],
        'password' : userData['password']
      },
      message: "Register is completed"
    }));
  }
})

function showOne(req, res) {

  var letter = req.params['letter'];

  var reply = {};

  if (data.frequencies[letter] === undefined) {
    reply.status = 'not a valid letter';
  } else {
    reply.status = 'success';
    reply.letter = letter;
    reply.freq = data.frequencies[letter];
  }

  res.send(reply);
}
