var express = require('express');
var app = express();

var server = app.listen(3000, listen);

function listen() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Listening at http://' + host + ':' + port);
}

app.get('/thing', doThing);

function doThing(req, res) {
  var name = req.query['name'];
  res.send("Thanks for doing your thing, " + name);
}
