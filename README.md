# nodejs-course

#Examples (Nodjs basics)
- Hello World Node + Express
- Serving static files
- Route basics
- “RESTful” routes
- Simple API
- Saving data to JSON file
- Proxy for scraping html

#setup
- step 1 downlaod nodjs at https://nodejs.org/en/
- step 1.1 if you use linux use command :
```
    sudo apt install curl;
    
    sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -;
    
    sudo apt update;
    sudo apt install nodejs -y;
```
- step 2 check nodjs version
```
    node -v
    v10.15.3
```