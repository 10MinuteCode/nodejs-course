var express = require('express');
var app = express();

var server = app.listen(3000, listen);

function listen() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Listening at http://' + host + ':' + port);
}

app.get('/thing/:name/:num', doThing);

function doThing(req, res) {
  var name = req.params['name'];
  var num = req.params['num'] || 1;
  var output = '';
  for (var i = 0; i < num; i++) {
    output += "Thanks for doing your thing, " + name + '<br/>';
  }
  res.send(output);
}
